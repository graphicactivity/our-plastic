// Require

var gulp    = require('gulp'),
    gutil   = require('gulp-util'),
    sass    = require('gulp-sass'),
    concat  = require('gulp-concat'),
    prefix  = require('gulp-autoprefixer'),
    postcss = require('gulp-postcss'),
    rename  = require('gulp-rename'),
    uglify  = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps');
    tailwindcss = require('tailwindcss');

gulp.task('css', function () {
  return gulp.src('src/styles/main.css')
    .pipe(postcss([
      tailwindcss('tailwind.js'),
      require('autoprefixer'),
    ]))
    .pipe(gulp.dest('web/assets/css'));
});

gulp.task('scripts', function() {
  return gulp.src('src/scripts/main.js')
  .pipe(concat('main.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('web/assets/js'));
});

gulp.task('watch', function () {
  gulp.watch('src/styles/*.css', ['css']);
  gulp.watch('tailwind.js', ['css']);
  gulp.watch('src/scripts/*.js', ['scripts']);
});
